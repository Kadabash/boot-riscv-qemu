boot:
	riscv64-unknown-elf-as boot.S -o boot.o
	riscv64-unknown-elf-ld --script boot.lds -o boot.elf boot.o


.PHONY: run
run:
	/usr/bin/qemu-system-riscv64 -kernel boot.elf
