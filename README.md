# Minimal RISC-V Assembly for starting in QEMU

Boot a minimal assembly program in a QEMU RISCV64 guest.

As a demonstration, this program loads compiled RISC-V machine instructions
into memory and executes them.

## Requirements:

* RISC-V toolchain, install following instructions on https://github.com/riscv/riscv-gnu-toolchain,
  building the Newlib cross-compiler (not the Linux one) so that `riscv64-unknown-elf-as` is available after installation.

* `qemu-system-riscv64`<br>
  NOTE: The `qemu-system-riscv64` from the `riscv-qemu` GitHub repository currently appears not to handle memory stores and loads correctly.
  Using the one provided in Ubuntu 18.10 with package `qemu-system-misc` works, however.

## Compiling & Running

Compile with `make boot` and run in QEMU with `make run`.
See that registers have really been modified by typing `info registers` into the QEMU console.
Using `memsave` it can be checked that data is really written to memory.



## Notes

* It seems that the machine will put the value 2 into the `mcause` register if it encounters an invalid instruction.
  This is described in the RISC-V Privileged Architecture manual, table 3.6, and also occurs when all instructions have been executed.
